
# Generic Makefile to compile a collection of pd-pure objects to a shared
# library which can be loaded with Pd's -lib option.

# Package name and version:
dist = pd-lv2plugin-$(version)
version = 0.3

# The filename extension for Pd object libraries depends on your operating
# system. Edit this as needed.
PDEXT       = .pd_linux

# Other platform-specific information can be obtained from Pure's pkg-config.
DLL         = $(shell pkg-config pure --variable DLL)
PIC         = $(shell pkg-config pure --variable PIC)
shared      = $(shell pkg-config pure --variable shared)
purelib     = $(shell pkg-config pure --variable libdir)/pure

pure_incl   = $(shell pkg-config pure --cflags)
pure_libs   = $(shell pkg-config pure --libs)

# Compilation and linker flags. Adjust these as needed.
CFLAGS = -g -O2
ALL_CFLAGS = $(PIC) $(CFLAGS) $(CPPFLAGS) -I. $(pure_incl) -Ipd
ALL_LDFLAGS = $(LDFLAGS) $(pure_libs) $(LIBS)

# Pd flavour (e.g., pd, pd-extended, pd-l2ork, etc.)
PD=pd

# Pd executable name variants.
PDEXE=$(subst pd-extended,pdextended,$(PD))

# Try to guess the Pd installation prefix:
prefix = $(patsubst %/bin/$(PDEXE),%,$(shell which $(PDEXE) 2>/dev/null))
ifeq ($(strip $(prefix)),)
# Fall back to /usr/local.
prefix = /usr/local
endif

# Installation goes into $(libdir)/$(PD), you can also set this directly
# instead of $(prefix).
libdir = $(prefix)/lib

# Pd library path.
pdlibdir = $(libdir)/$(PD)

# Install dir for the externals and accompanying stuff.
pdextradir = $(pdlibdir)/extra/lv2plugin~

# Helper libraries.
solibs = pdstub$(DLL)

ifeq ($(DLL),.dylib)
PDEXT = .pd_darwin
endif

%$(DLL): %.c
	gcc $(shared) $(ALL_CFLAGS) $< -o $@ $(ALL_LDFLAGS)

all: $(solibs) lv2plugin~$(PDEXT) lv2plugin~-meta.pd

lv2plugin~-meta.pd: lv2plugin~-meta.pd.in Makefile
	sed -e "s?@version@?$(version)?g" < $< > $@

ifeq ($(DLL),.dylib)
# OS X: We need the -undefined dynamic_lookup linker flag here to link back
# into the Pd executable.
extralibs = -Wl,-undefined -Wl,dynamic_lookup $(addprefix $(purelib)/, lilv$(DLL))
else
# Linux: This requires a linker that handles the -rpath option. Otherwise you
# may have to add the $(purelib) and $(pdextradir) directories to your system
# dll search path so that the linked library modules are found when the
# lv2plugin~ module is loaded by Pd.
extralibs = -Wl,-rpath=$(purelib) $(addprefix $(purelib)/, lilv$(DLL))
endif

# This links the compiled Pure code and loader to a shared library object with
# the proper extension required by Pd.
lv2plugin~$(PDEXT): lv2plugin~.o loader.o
	gcc $(PIC) $(shared) $^ $(extralibs) -o lv2plugin~$(DLL)
	test "$(DLL)" = "$(PDEXT)" || mv lv2plugin~$(DLL) lv2plugin~$(PDEXT)

# This uses the Pure interpreter to compile our pd-pure objects to native code.
# Note that the --main option is necessary to prevent name clashes and allow
# the module to coexist with other modules of its kind.
lv2plugin~.o: lv2plugin~.pure
	pure $(PIC) -c $^ -o $@ --main=__lv2plugin_tilde_main__

# Compile a minimal loader module which is needed to interface to Pd and
# register the object classes with pd-pure.
loader.o: loader.c
	gcc $(PIC) $(pure_incl) -Ipd -c $< -o $@

clean:
	rm -Rf *.o *$(DLL)* *$(PDEXT)

install:
	test -d "$(DESTDIR)$(pdextradir)" || mkdir -p "$(DESTDIR)$(pdextradir)"
	cp COPYING README.md *.pure examples/*.pd lv2plugin~$(PDEXT) lv2plugin~-meta.pd "$(DESTDIR)$(pdextradir)"

uninstall:
	rm -rf "$(DESTDIR)$(pdextradir)"

DISTFILES = COPYING Makefile README.md *.c *.pure *-meta.pd.in pd/*.h examples/*.pd

dist:
	rm -rf $(dist)
	for x in $(dist) $(addprefix $(dist)/, pd examples); do mkdir $$x; done
	for x in $(DISTFILES); do ln -sf "$$PWD"/$$x $(dist)/$$x; done
	rm -f $(dist).tar.gz
	tar cfzh $(dist).tar.gz $(dist)
	rm -rf $(dist)

distcheck: dist
	tar xfz $(dist).tar.gz
	cd $(dist) && make && make install DESTDIR=./BUILD
	rm -rf $(dist)
