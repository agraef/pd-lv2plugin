Title:  lv2plugin~
Author: Albert Gräf <aggraef@gmail.com>
Date:   2014-02-14

lv2plugin~
==========

This is a Pd external which provides a simple LV2 host so that you can run LV2
plugins inside Pd. The overall operation is similar to the LADSPA plugin~
external, but it works with [LV2](http://lv2plug.in/), the new Linux plugin
standard. It supports both audio and MIDI plugins. lv2plugin~ is based on
[lilv](http://drobilla.net/software/lilv/), the reference LV2 host library, so
it should work with almost any LV2 plugin.

License
=======

lv2plugin~ is Copyright (c) 2014-2016 by Albert Gräf. It is distributed under
the 3-clause BSD license, please check the COPYING file for details.

Installation
============

lv2plugin~ is written in the author's [Pure](http://purelang.bitbucket.org/)
programming language, so you need to have the Pure interpreter installed, as
well as the pd-pure plugin loader and the pure-lilv module. You can find all
of these on the Pure website. You'll also need the lilv library itself and the
LV2 headers, which can be downloaded at <http://drobilla.net/software>, and
should also be readily available on most Linux distributions.

Users of Arch Linux may want to check the Arch User Repositories (AUR), as we
try to maintain a fairly complete and recent collection of Pure- and
Pd-related packages there.

Mac OS X users can find a ready-made binary package for 64 bit Intel systems
here: [pd-lv2plugin-0.3-macosx-x86_64.zip](https://bitbucket.org/agraef/pd-lv2plugin/downloads/pd-lv2plugin-0.3-macosx-x86_64.zip). The zip file contains the
`lv2plugin~` directory which you'll have to copy to a directory on Pd's library
path (usually `/Library/Pd` for system-wide and `~/Library/Pd` for personal
installation on the Mac). (You'll also need the Pure interpreter which is
available in [MacPorts](http://www.macports.org/). Please check the [Pure On
Mac OS X](https://bitbucket.org/purelang/pure-lang/wiki/PureOnMacOSX) wiki
page for details.)

To compile the software yourself, check the included Makefile for settings
that might need to be adjusted for your system, then run:

    make
    sudo make install

This works with vanilla Pd. If you're running pd-extended or pd-l2ork then
during installation you need to specify the Pd flavor using the `PD` make
variable, e.g.:

    sudo make install PD=pd-extended

Or just copy the `extra/lv2plugin~` folder from the vanilla installation, the
plugin binary should work with all Pd flavors.

Getting Started
===============

A help patch is available which demonstrates how to use the external. Open the
Pd help browser and look for a section named `lv2plugin~`. You'll find a patch
named `lv2plugin~-help` there.

The external and related patches are installed in the `extra/lv2plugin~`
directory of your Pd installation (usually under `/usr/lib/pd` or similar).
The installation also includes two little helper patches `midi-input.pd` and
`midi-output.pd` which translate between Pd's MIDI input and output and the
MIDI message format understood by `lv2plugin~`. If you want to use these,
you'll have to copy them somewhere where Pd finds them, or put the
`extra/lv2plugin~` directory on your Pd search path.

Usage
=====

The external is to be invoked with the URI or the name of the LV2 plugin to be
instantiated. To get a list of known LV2 plugins on your system, use the
`listplugins` message in the help patch. (You can also just create an
`lv2plugin~` object without arguments, hook it up to a `print` object and send
it the `listplugins` message to do this.) Each printed line will show
something like:

    plugin subtractive http://faustlv2.bitbucket.org/subtractive

Here, `subtractive` is the name and `http://faustlv2.bitbucket.org/subtractive`
the URI of the plugin. You can use either as an argument of `lv2plugin~` when
instantiating the plugin. If the name is a valid Pure symbol then it can be
used as is; otherwise (or if using the URI) you must enclose the argument in
double quotes.

Instantiated plugins always have a pair of control in- and outlets on the
left-hand side. The leftmost *inlet* can be used to change control values,
send MIDI data and to execute a number of special operations as explained
below. The leftmost *outlet* sends `control` messages for output controls of
the plugin (if it has any), as well as MIDI data and other output from the
special operations. The remaining inlets and outlets of the `lv2plugin~`
instance are the audio inputs and outputs of the plugin. Thus, if an LV2
plugin has n audio inputs and m audio outputs (n,m >= 0) then the
corresponding `lv2plugin~` instance will have n+1 inlets and m+1 outlets,
respectively.

The `lv2plugin~` external understands the following special messages:

  * `active` changes the activation status of a plugin. It takes one numeric
    argument which should be 0 to deactivate and nonzero to activate the
    plugin.  Note that it's entirely up to the plugin how this feature is
    implemented, if at all. ([faust-lv2][] plugins will generally bypass or
    mute the plugin if it's deactivated, depending on their number of audio
    inputs and outputs.)

  * `control` changes control values. It takes two arguments, the index or name
    of the control port (as shown by `info`, see below) and the new value (a
    number). This kind of message may also be output to the object's left
    control outlet if the plugin has control output ports.

    **NOTE:** As Pd makes it hard to enter symbols containing spaces and other
    special characters, `lv2plugin~` actually accepts "mangled" control names
    in which each contiguous sequence of non-alphanumeric characters in the
    control name is replaced with a single dash. Thus, e.g., the control name
    `Dry/Wet Mix` should be entered as `Dry-Wet-Mix`. The same format is also
    used for control output and control labels in the generated GUIs.

    Also note that `lv2plugin~` always employs human-readable port names in
    its user interface, rather than the internal LV2 port symbols which may
    look rather funky for some plugins. In contrast to the latter, the former
    are not guaranteed to be unique, so a plugin may have different controls
    with the same (clear and mangled) control names. In such a case you will
    have to use the index of the control to specify the control value to be
    changed in an unambiguous way.

  * `gui` generates a GOP patch with a generic GUI for the plugin. See *GUI
    Instantiation* below.

  * `info` prints information about the LV2 ports of the plugin. This includes
    all control, audio and MIDI input and output ports of the plugin. The
    listed information shows the port numbers, category (`in`, `out` or
    `inout`), type (`control`, `audio`, `midi` and `cv`) and (unmangled) name
    of the port. (CV a.k.a. "control voltage" ports are treated like audio
    ports but carry control information, so you probably don't want to hook
    them up to your DAC.) Control ports also have their ranges (min and max
    value), default and current values listed. For instance, the `amp` plugin
    from the [faust-lv2][] package will give you something like:

        port 0 in control bass -20 20 0 0
        port 1 in control treble -20 20 0 0
        port 2 in control gain -96 10 0 0
        port 3 in control balance -1 1 0 -0.488189
        port 4 out control left -96 10 -96 -102.837
        port 5 out control right -96 10 -96 -108.655
        port 6 in audio in0
        port 7 in audio in1
        port 8 out audio out0
        port 9 out audio out1
        port 10 in midi midiin

  * `listplugins` lists the names and URIs of all known LV2 plugins on your
    system.

  * `listpresets` lists all known presets of the plugin. You can also save
    presets and load them with the `savepreset` and `preset` messages, please
    check *Preset Management* below for details.

  * `reset` resets all control values of a plugin to their defaults.

[faust-lv2]: https://bitbucket.org/agraef/faust-lv2

MIDI Input and Output
---------------------

Besides these, `lv2plugin~` also understands MIDI messages in the following
special format (`n` denotes a note or controller number, `v` the velocity or
controller value, `c` the MIDI channel number in the range 1..16):

Message                  | Meaning
------------------------ | -------------------------
`ctl v n c`              | control change
`note n v c`             | note
`pgm n c`                | program change
`polytouch v n c`        | key pressure
`touch v c`              | channel pressure
`bend v c`               | pitch bend (14 bit value)
`start`, `stop`, `cont`  | system realtime messages
`sysex b1 b2 ...`        | system exclusive message

MIDI input in this format will be passed as proper MIDI messages to the
plugin's first MIDI input (if it has any). If the plugin has MIDI output
ports, MIDI messages emitted by the plugin will be translated back to the
above format and output on the object's left control outlet.

Note that most of these messages closely correspond to the inlets and outlets
of Pd's MIDI objects, so translating between these and the `lv2plugin~` format
is fairly straightforward. A few special system realtime and system exclusive
messages are also understood, which don't have a direct counterpart in Pd.
The distribution includes two little helper patches (`midi-input.pd` and
`midi-output.pd` in the `examples` folder) which handle the conversion, you
might want to look at these and adjust them for your needs as you see fit.

Preset Management
-----------------

`lv2plugin~` has some rudimentary preset management functionality which can be
invoked with the following messages:

  * `listpresets` outputs the names and URIs of all known presets for the
    instantiated plugin. E.g., with the `MDA EPiano` plugin from the
    [MDA LV2 plugins][] you'll get something like:

        preset Default http://drobilla.net/plugins/mda/presets#EPiano-default
        preset Bright http://drobilla.net/plugins/mda/presets#EPiano-bright
        preset Autopan http://drobilla.net/plugins/mda/presets#EPiano-autopan
        preset Tremolo http://drobilla.net/plugins/mda/presets#EPiano-tremolo
        preset Mellow http://drobilla.net/plugins/mda/presets#EPiano-mellow

  * `savepreset` with the desired preset name as argument saves the preset in
    your `~/.lv2` folder, from where it can then be reloaded using the
    `preset` message.

Please check the included help patch for examples showing how to use this.

[MDA LV2 plugins]: http://drobilla.net/software/mda-lv2/

GUI Instantiation
-----------------

As of version 0.2, `lv2plugin~` also provides a built-in capability to
generate generic GUIs for LV2 plugins using Pd GOP (graph-on-parent)
subpatches. Please see the help patch for an example. The GUI for each plugin
has two special controls (a bang and a checkbox) in its title bar to send the
special `reset` and `active` messages, as well as pairs consisting of a
horizontal slider and a number entry for each control port, which are used to
show and edit the corresponding control values. (This works with both input
and output control ports; the latter are shown with a special background
color.)

To utilize this in your patches, simply create a one-off subpatch named after
the plugin (i.e., `pd name` for the `lv2plugin~ name` plugin instance). If the
plugin instance already exists, you then have to send the plugin the `gui`
message in order to populate the subpatch and set its GOP area. (This step
isn't necessary if you create the plugin instance after the subpatch, or if
the hosting patch is reloaded, as each plugin instance takes care of creating
its GUI at creation time if a corresponding one-off subpatch is available.)

Note that the name of the GUI subpatch should match the *name* of the plugin
(as given by the `listplugins` message), even if the plugin is instantiated
using its URI. We also refer to this as the *instance name* of the plugin.
The instance name is also used as a prefix on the send and receive symbols of
the GUI elements of the patch, so that the control ports don't get mixed up
between different plugin instances even if the ports happen to have the same
names.

If you need several instances of the same plugin in a patch, you can
distinguish them by specifying the instance name explicitly as the optional
second argument of the `lv2plugin~` object. This can also be used if you want
to name the GUI patch differently for some reason. E.g.,

    lv2plugin~ amp "amplifier-gui-1"

creates the `amp` plugin with `amplifier-gui-1` as its instance name, so the
GUI for this instance would go into the `pd amplifier-gui-1` subpatch.

Note that the contents of the GUI subpatch is regenerated automatically every
time its parent patch is loaded. You may want to prevent this, e.g., when
you've edited the GUI patch manually. For that it is sufficient to just rename
the GUI subpatch before you save its parent patch.

Feedback and Bug Reports
========================

As usual, bug reports, patches, feature requests and other comments and source
contributions are more than welcome. Just drop me an email, file an issue at
the tracker or send me a pull request on lv2plugin's Bitbucket page
<https://bitbucket.org/agraef/pd-lv2plugin>.

Enjoy! :)

Albert Gräf <aggraef@gmail.com>
